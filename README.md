# README #

This Repository was created to manage a solution requested for Employment Assessment purposes.

### What is this repository for? ###

* Create a Web API that returns "Hello World"
* Use **Autofac** for constructor injection (using interfaces) between the api and a mocked data layer 
* The mocked data layer with consist of Business Objects and interfaces
* Consume the API from a webpage using **jQuery**. The result should show "hello world" 10 times
* Make sure the **C# code is async** all the way. This may require you to fake the async in the mocked infrastructure layer.

* The solution be organized as follows:

	| sample.API (project)
	
	| Sample.Infrastucture (project) (Mocked repo method can just return "Hello World" asynchronously)
	
	| Sample.Domain (project)
	
	|--| Entities (folder) (contains Business Objects)
	
	|--| Abstract (folder) (contains interfaces)
	
	| Sample.Presentation (project) (Consume the API from a webpage using jQuery)
	
* Optional Technologies to incorporate:
	- **.net core**
	- Logging using **nlog**
	- (.net core) Global Exception handling using middleware that logs any globally thrown exceptions (in the request lifecycle) to nlog. the new middleware can be added via an extension method during Startup using an extension method.
	- Please use **Visual Studio 2017**. Code must compile and run without errors. If you do not have it installed, the Community Edition is free.
	
	Hint: When using autofac, you will have to register your webapi and any assemblies that your webapi will need.
	
### View Working Demo ###

demo of the API here: [Azure Deployed API](http://helloworldapi20170821124849.azurewebsites.net/api/HelloWorld)

demo of the APP here" [Azure Deployed APP](http://helloworldpresentation20170821125441.azurewebsites.net/)