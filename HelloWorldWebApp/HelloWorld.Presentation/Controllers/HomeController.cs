﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HelloWorld.Presentation.Controllers
{
    public class HomeController : Controller
    {
        #region Properties
        private readonly ILogger<HomeController> _logger;
        #endregion

        #region Constructors
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        #endregion

        #region Actions
        public IActionResult Index()
        {
            _logger.LogInformation("Navigated to Index");
            return View();
        }

        public IActionResult Error()
        {
            _logger.LogInformation("Error has occurred");
            return View();
        }
        #endregion
    }
}
