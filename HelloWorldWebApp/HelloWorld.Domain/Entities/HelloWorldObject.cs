﻿using System;

namespace HelloWorld.Domain
{
    public class HelloWorldObject
    {
        #region Properties
        /// <summary>
        /// Stores a Message 
        /// </summary>
        public string Message { get; set; }
        #endregion

        #region Constructors
        public HelloWorldObject()
        {
            Message = String.Empty;
        }
        #endregion
    }
}
