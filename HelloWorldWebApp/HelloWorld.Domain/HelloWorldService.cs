﻿using System.Threading.Tasks;

namespace HelloWorld.Domain
{
    public class HelloWorldService : IHelloWorldService
    {
        #region Properties
        /// <summary>
        /// used to call data store for Hello World Object
        /// </summary>
        private IHelloWorldRepository _helloWorldRepository { get; set; }
        #endregion

        #region Constructors
        public HelloWorldService(IHelloWorldRepository helloWorldRepository)
        {
            _helloWorldRepository = helloWorldRepository;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Asynchronously get Hello World from Repository
        /// </summary>
        /// <returns></returns>
        public async Task<string> HelloWorldAsync()
        {
            HelloWorldObject helloWorldObject = await _helloWorldRepository.GetHelloWorldObjectAsync();
            return helloWorldObject.Message;
        }
        #endregion
    }
}
