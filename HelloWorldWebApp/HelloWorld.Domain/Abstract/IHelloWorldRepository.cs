﻿using System.Threading.Tasks;

namespace HelloWorld.Domain
{
    public interface IHelloWorldRepository
    {
        /// <summary>
        /// Asynchronously call to get Hello World Object from data store
        /// </summary>
        /// <returns></returns>
        Task<HelloWorldObject> GetHelloWorldObjectAsync();
    }
}
