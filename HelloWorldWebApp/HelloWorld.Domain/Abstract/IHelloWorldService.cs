﻿using System.Threading.Tasks;

namespace HelloWorld.Domain
{
    public interface IHelloWorldService
    {
        Task<string> HelloWorldAsync();
    }
}
