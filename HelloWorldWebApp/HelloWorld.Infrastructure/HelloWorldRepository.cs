﻿using HelloWorld.Domain;
using System.Threading.Tasks;

namespace HelloWorld.Infrastructure
{
    public class HelloWorldRepository : IHelloWorldRepository
    {
        /// <summary>
        /// Call this method to get an instance of the Hello World Object
        /// </summary>
        /// <returns></returns>
        public async Task<HelloWorldObject> GetHelloWorldObjectAsync()
        {
            HelloWorldObject helloWorldObject = new HelloWorldObject()
            {
                //Get from the data store
                Message = "Hello World"
            };

            return await Task.FromResult(helloWorldObject);
        }
    }
}
