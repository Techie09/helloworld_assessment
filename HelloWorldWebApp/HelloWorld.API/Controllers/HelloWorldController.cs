﻿using System.Threading.Tasks;
using HelloWorld.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;

namespace HelloWorld.API.Controllers
{
    [Route("api/[controller]")]
    public class HelloWorldController : Controller
    {
        private IHelloWorldService _helloWorldService;
        private ILogger<HelloWorldController> _logger;

        public HelloWorldController(IHelloWorldService helloWorldService, ILogger<HelloWorldController> logger)
        {
            _helloWorldService = helloWorldService;
            _logger = logger;
        }

        // GET api/HelloWorld
        [HttpGet]
        public async Task<string> Get()
        {
            _logger.LogInformation("GET: HelloWorld");
            return await _helloWorldService.HelloWorldAsync();
        }
    }
}
